package com.yamani.oms.rest.api.gsdm.order;

import java.util.ArrayList;
import java.util.List;


  /**
  *  0-1 Knapsack Problem Solver (Algorithm using Dynamic Programming)
  *  
  **/
  public class KnapsackProblemSolver {
    
    private double lastMaxKnapsackItemProfit;
    private List<Integer> lastListOfUsedItemsIndex;
    
    public KnapsackProblemSolver() {
      this.lastListOfUsedItemsIndex = new ArrayList<Integer>();
    }
    
    /**
     * Retrieves a <long> array of knapsack items solving MaxWeight
     *
     * @param itemWeights
     * @param knapsackItemPositions
     * @return
     */
    public double[] getKnapsackItems(double[] itemWeights, List<Integer> knapsackItemPositions) {
      int numOfUsedItems = knapsackItemPositions.size();
      double[] knapsackItems = new double[numOfUsedItems];
      int i=0;
      for(Integer itemPosition: knapsackItemPositions) {
        knapsackItems[i] = itemWeights[itemPosition];
        i++;
      }
      return knapsackItems;
    }
    
    /**
     * 
     * Solves Knapsack Problem
     *  
     * @param itemWeights List of item weight (cost), used as item value/profit too
     * @param knapsackMaxWeight capacity (target weight)
     */
    public List<Integer> solveKnapsack(double[] itemWeights, int knapsackMaxWeight,int numOfItems) throws Exception {
      return solveKnapsack(itemWeights,itemWeights,knapsackMaxWeight,numOfItems);
    }
    
    /**
     * 
     * Solves Knapsack Problem
     *  
     * @param itemProfits List of item value (profit)
     * @param itemWeights List of item weight (cost)
     * @param knapsackMaxWeight capacity (target weight)
     */
    public List<Integer> solveKnapsack(double[] itemProfits,double[] itemWeights, int knapsackMaxWeight,int numOfItems) throws Exception {
      List<Integer> listOfUsedItemsIndex = new ArrayList<Integer>();
      double[][] costMatrix = getCostMatrix(itemProfits, itemWeights, knapsackMaxWeight, numOfItems);
      listOfUsedItemsIndex = getPositionsOfUsedItems(itemWeights, costMatrix, numOfItems, knapsackMaxWeight);
      lastMaxKnapsackItemProfit = costMatrix[numOfItems-1][knapsackMaxWeight];
      lastListOfUsedItemsIndex.addAll(listOfUsedItemsIndex);
      return listOfUsedItemsIndex;
    }
    
    /**
     * Cost Matrix created by dynamic programming
     * MaxProfit: costMatrix[numOfItems-1][knapsackMaxWeight]
     *
     * @param itemProfits List of item value (profit)
     * @param itemWeights List of item weight (cost)
     * @param knapsackMaxWeight capacity (target weight)
     * @return CostMatrix (2-d long array)
     */
    public double[][] getCostMatrix(double[] itemProfits,double[] itemWeights, int knapsackMaxWeight,int numOfItems) throws Exception {
      double[][] costMatrix = new double[numOfItems][((int)knapsackMaxWeight+1)];
      
      if(knapsackMaxWeight <= 0) throw new Exception("Invalid knapsackMaxWeight provided: " + knapsackMaxWeight);
      if(numOfItems <= 0) throw new Exception("Invalid numer of items provided: " + numOfItems);
      
      for(int i = 0; i < numOfItems; i++) {
        for(int w = 0; w < knapsackMaxWeight+1; w++) {
          // Consider every weight...
          // If the current item weight > current knapsack weight (w), then we cannot put item in knapsack
          // Else if the current item weight > current knapsack weight(w), we can add this item's weight
          if(itemWeights[i] > w) {
            costMatrix[i][w] = (i==0) ? 0L : costMatrix[i-1][w];
          } else {
            // c[i][j] = max(c[i-1][j],v[i] +c[i-1][j-w[i]])
            int curretWeightMinusItemWeight = (new Double((new Integer(w)).doubleValue()-itemWeights[i])).intValue();
            double previousItemWeightCost = ((i == 0) ? itemProfits[i] : (itemProfits[i] + costMatrix[i-1][curretWeightMinusItemWeight]));
            costMatrix[i][w] = ((i == 0) ? previousItemWeightCost : Math.max(costMatrix[i-1][w], previousItemWeightCost));
          }
        }
      }
      return costMatrix;
    }
    
    /**
     * Gets a list of item positions used to fill knapsack with MaxWeight 
     *  
     * @param itemWeights List of item weight (cost)
     * @param costMatrix create by dynamic programming solution
     * @param numOfItems (count)
     * @return
     */
    public List<Integer> getPositionsOfUsedItems(double[] itemWeights, double[][] costMatrix, int numOfItems, int knapsackMaxWeight) {
      List<Integer> markedItems = new ArrayList<Integer>();
      int i = numOfItems - 1;
      int currentWeight = knapsackMaxWeight;
      while( i >= 0 && currentWeight >= 0) {
        if( (i==0 && costMatrix[i][currentWeight] > 0L)) {
          markedItems.add(i);
        } else if(i > 0 && costMatrix[i][currentWeight] != costMatrix[i-1][currentWeight]) {
          markedItems.add(i);
          currentWeight -= itemWeights[i];
        }
        i--;
      }
      return markedItems;
    }
    

    /**
     * @return the maxKnapsackItemProfit
     */
    public double getLastMaxKnapsackItemProfit() {
      return lastMaxKnapsackItemProfit;
    }

    /**
     * @return the lastListOfUsedItemsIndex
     */
    public List<Integer> getLastListOfUsedItemsIndex() {
      return lastListOfUsedItemsIndex;
    }

    /**
     * @param lastListOfUsedItemsIndex the lastListOfUsedItemsIndex to set
     */
    public void setLastListOfUsedItemsIndex(List<Integer> lastListOfUsedItemsIndex) {
      this.lastListOfUsedItemsIndex = lastListOfUsedItemsIndex;
    }
    
    
  }