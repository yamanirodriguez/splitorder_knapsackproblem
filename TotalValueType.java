// Copyright (c) 2003-2017, One Network Enterprises, Inc. All rights reserved.

package com.onenetwork.oms.rest.api.gsdm.order;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.SerializationUtils;
import org.json.JSONException;

import com.onenetwork.oms.rest.api.gsdm.GSDMUtil;
import com.onenetwork.supplychaincore.model.EnhancedOrder;
import com.onenetwork.supplychaincore.model.OrderLine;
import com.transcendsys.platform.web.util.StringUtil;

/**
   * 
   *  Group existing OrderLines by TotalValue <= 4000 per order
   *  Focus: TotalValue of OrderLines->FieldValue in each existing order (Subsets sum)
   *  From each existing Order, create new orders in which the OrderLines->FieldTotal <= TotalValue
   *  isApplicable: if two or more orders result from the process(overall), then it is applicable
   *
   *  Config Example: {"ModelLevel": "OrderLine", "Type": "TotalValue", "TotalValue": 4000, "Field": "UnitPrice", "RulePriority":7}
   */
  public class TotalValueType implements OrderSplitType {
    private List<EnhancedOrder> orders;
    private SplitOrderRule rule;
    
    public TotalValueType() {
      this.rule = null;
    }
    
    public TotalValueType(SplitOrderRule rule) throws SplitOrderException, IllegalAccessException, JSONException {
      this.rule = rule;
      if(!isRuleValid(this.rule)) {
        throw new SplitOrderException("Invalid rule. Rule: " + GSDMUtil.toJSONFromObject(this.rule));
      }
    }
    
    /**
     * @see com.onenetwork.oms.rest.api.gsdm.order.OrderSplitter.OrderSplitType#split(com.onenetwork.supplychaincore.model.EnhancedOrder)
     */
    @Override
    public List<EnhancedOrder> split(EnhancedOrder order) throws Exception {
      if(order == null) throw new Exception("No order provided");
      if(rule == null) throw new Exception(this.getClass().getSimpleName() + " not instantiated with Rule");
      
      List<OrderLine> orderLines = order.getOrderLines();
      orders = new ArrayList<EnhancedOrder>();
      //orders.add(order); // Add current order to orders list
      
      /*
       * TODO 
       * 1) Get array of fieldvalues
       * 2) Get list of order lines totaling TotalValue (using Knapsack algorithm)
       * 3) Clone order header, clear order lines, and add new orders lines
       * 4) Add new order to new list of orders
       * 5) Remove used order lines and fetch other groups of orders lines until orderLines list is empty
       * 
       */

      while(orderLines.size() > 0) {
        // Get array of fieldValues and count
        int numOfFieldValues = orderLines.size();
        double[] lineFieldValues = getLineFieldValues(orderLines);
        System.out.println("TotalValue: " + rule.getTotalValue().intValue() + " numOfFieldValues: " + numOfFieldValues + " lineFieldValues: " );
        for(double val: lineFieldValues) {
          System.out.println(val);
        }
        
        // Get list of orderLines whose Field values total sum, x <= TotalValue
        KnapsackProblemSolver knapsack = new KnapsackProblemSolver();
        List<Integer> listOfUsedItemsIndex = knapsack.solveKnapsack(lineFieldValues, rule.getTotalValue().intValue(), numOfFieldValues);
        System.out.println("listOfUsedItemsIndex: " + listOfUsedItemsIndex);
        if(listOfUsedItemsIndex.size() == 0) break;
        
        List<OrderLine> newListOfOrderLines = new ArrayList<OrderLine>();
        for(Integer index: listOfUsedItemsIndex) {
          OrderLine tmpOrderLine = (OrderLine) SerializationUtils.clone(orderLines.get(index.intValue()));
          newListOfOrderLines.add(tmpOrderLine);
          orderLines.remove(index.intValue());
        }
        
        //Clone Order Header and add new lines
        EnhancedOrder newOrder = (EnhancedOrder) SerializationUtils.clone((EnhancedOrder) order);
        newOrder.setOrderNumber("NewOrder#" + (orders.size()+1));
        newOrder.getOrderLines().clear();
        newOrder.getOrderLines().addAll(newListOfOrderLines);
        
        double[] selectedItemValues = knapsack.getKnapsackItems(lineFieldValues, listOfUsedItemsIndex);
        System.out.println("selectedItemValues: ");
        for(double val: selectedItemValues) {
          System.out.println(val+ " ");
        }
        
		// Replace /w async logging
        System.out.println("NewOrder# " + (orders.size()+1) + " Size of orders: " + (orders.size()+1));
        orders.add(newOrder);
      }
      
      return orders;
    }

    /**
     * Get fieldValues in an OrderLine
     *
     * @param orderLines
     * @param i
     * @return array of <long> fieldValues
     * 
     * @throws Exception
     */
    private double[] getLineFieldValues(List<OrderLine> orderLines) throws Exception {
      double[] lineFieldValues = new double[orderLines.size()];
      int i=0;
      
      for(OrderLine line: orderLines) {
        try {
            Double fieldValue = getFieldValue(line, rule);
            lineFieldValues[i] = fieldValue.doubleValue();
            i++;
            System.out.println("i: " + i + " FieldValue: " + fieldValue); //TODO REMOVE
        } catch (Exception e) {
          //e.printStackTrace();//TODO remove
          throw new Exception(e.getMessage());
        }
      }
      return lineFieldValues;
    }
    /**
     * @see com.onenetwork.oms.rest.api.gsdm.order.OrderSplitter.OrderSplitType#split(java.util.List)
     */
    @Override
    public List<EnhancedOrder> split(List<EnhancedOrder> orders) throws Exception {
      if(CollectionUtils.isEmpty(orders)) throw new Exception("Orders list is empty.");
      if(rule == null) throw new Exception(this.getClass().getSimpleName() + " not instantiated with Rule");
      List<EnhancedOrder> newOrders = new ArrayList<EnhancedOrder>();
      for(EnhancedOrder order: orders) {
        newOrders.addAll(split(order));
      }
      orders.clear(); // Remove current list; the newOrders List will become the current list 
      return newOrders;
    }
    /**
     * @see com.onenetwork.oms.rest.api.gsdm.order.OrderSplitter.OrderSplitType#isRuleValid(com.onenetwork.oms.rest.api.gsdm.order.SplitOrderRule)
     */
    @Override
    public boolean isRuleValid(SplitOrderRule rule) {
      if(rule != null && !rule.getType().equals(OrderSplitter.TOTAL_VALUE_TYPE)
        || StringUtil.isNullOrBlank(rule.getModelLevel()) 
        || !rule.getModelLevel().equals("OrderLine")
        || StringUtil.isNullOrBlank(rule.getField())
        || rule.getRulePriority() == null || rule.getRulePriority() < 0
        || rule.getTotalValue() == null || rule.getTotalValue().compareTo(0L) <= 0) {
          return false;
        }
      return true;
    }
    
    /**
     * @see com.onenetwork.oms.rest.api.gsdm.order.OrderSplitType#getRuleTypeFields(java.lang.String)
     */
    @Override
    public List<String> getRuleTypeFields() {
      List<String> ruleTypeFieldsList = new ArrayList<String>();
      // Populate list with common SplitOrderRule fields
      ruleTypeFieldsList.add("ModelLevel");
      ruleTypeFieldsList.add("Type");
      ruleTypeFieldsList.add("RulePriority");
      
      // Other fields unique to RuleType
      ruleTypeFieldsList.add("Field");
      ruleTypeFieldsList.add("TotalValue");
      
      return ruleTypeFieldsList;
    }

    /**
     * 
     * Gets field method and uses it to get field value 
     *
     * @param line
     * @param rule
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws SplitOrderException
     * 
     * @return FieldValue (Double)
     */
    public Double getFieldValue(OrderLine line, SplitOrderRule rule) 
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, SplitOrderException {
        Double fieldValue = null;
        String methodName = "get" + rule.getField();
        ModelFieldUtil fieldUtil = new ModelFieldUtil();
        Method getMethod = fieldUtil.getOrderLineGetMethod(line, rule);
        if(getMethod != null) {
          String retType = fieldUtil.getOrderLineGetMethodReturnType(getMethod);
          System.out.println("paramType: " + retType); // TODO REMOVE
          try {
            Object retObject = getMethod.invoke(line);
            if(retType.equals("double")) {
              fieldValue = Double.valueOf((double) retObject); 
            } else if(retType.equals("int")) {
              fieldValue = new Double((new Integer((int) retObject)).doubleValue());
            } else if(retType.equals("long")) {
              fieldValue = new Double((new Long((long) retObject)).doubleValue());
            } else if(retType.equalsIgnoreCase("java.lang.Long")) {
              fieldValue = new Double(((Long) retObject).doubleValue());
            } else {
              throw new SplitOrderException("Field retType("+retType+") is not supported. RuleType: " + rule.getType() + " RulePriority: " + rule.getRulePriority());
            }  
          } catch(IllegalArgumentException e) {
            throw new SplitOrderException("Rule field is not supported: " + rule.getField() );
          } catch(Exception e) {
            throw e;
          }
        } else {
          throw new SplitOrderException("Method ("+methodName+") was not found. Invalid Field: " + rule.getField() + ". RuleType: " + rule.getType() + " RulePriority: " + rule.getRulePriority());
        }
        
        return fieldValue;
    }
    
  }